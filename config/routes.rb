# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

resources :mytimes, :controller => 'mytimes' do
  get 'report', :on => :collection
end

# personalkosten
get 'differenz/upload' => 'csvupload#choose_file'
post 'differenz/upload' => 'csvupload#upload'
get 'differenz' => 'csvupload#index'
get 'differenz/export' => 'csvupload#export'
get 'differenz/export/:user_id' => 'csvupload#export'
get 'differenz/user/:user_id' => 'csvupload#month_times_per_cweek'
get 'differenz/user/:user_id/detail' => 'csvupload#month_times_per_day'

# usermapping staffnumer <=> user_id
get 'differenz/staffnumber' => 'csvupload#user_mapping'
get 'differenz/staffnumber/:user_id' => 'csvupload#user_mapping_detail'
post 'differenz/staffnumber/:user_id' => 'csvupload#edit_user_mapping'

get '/sendtimereport' => 'sendtimereport#weeklyreport'
