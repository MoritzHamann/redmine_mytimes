$(document).ready(function(){
    $(".kw-container").click(function(){
        
        var that = this;
        $(this).children(" .kw-days").slideToggle();
        var link_container = $(this).find(".kw-collapsible-link");
        toggleCollapseLink(link_container);
      }
    );
    
    // stop propagtion of click event from kw-days
    $(".kw-days").click(function(e){ console.log("kw-days");e.stopPropagation();});
  }
)

function toggleCollapseLink(link_container){
  var link = $(link_container).children()
  var collapsed = link.hasClass("collapsed");
  
  if (collapsed){
    link.removeClass("collapsed");
    link_container.parent().css("font-weight", "bold");
  }
  else {
    link.addClass("collapsed");
    link_container.parent().css("font-weight", "normal");
  }
}
  
