require File.dirname(__FILE__) + '/../test_helper'

class CsvEntryTest < ActiveSupport::TestCase

  # Replace this with your real tests.
  def test_save_csv
    line = ["202","1.1.2012","1.4","2.4","3.4"]
    assert CsvEntry.save_from_csv_array line
  end
end
