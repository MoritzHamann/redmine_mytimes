class Userstaffnumber < ActiveRecord::Base
  unloadable
  
  validates_numericality_of :staffnumber
  belongs_to :user
end
