class CsvEntry < ActiveRecord::Base
    unloadable

    belongs_to :user

    def hours
    # returns the number of hours for this entry
        return self.present + self.missing + self.improvment
    end
    
    def self.all_from_month(date)
        where('extract(month from date) = ? AND extract(year from date) = ?',date.month, date.year)
    end
    
    def self.by_user(user_id)
        where(:user_id => user_id)
    end
  
    def self.save_with_staff_number(values)
    # function uses values (hash) with the staffnumber to create or update a new csventry
        if Userstaffnumber.exists?(:staffnumber => values[:staffnumber])
            user = Userstaffnumber.where(:staffnumber => values[:staffnumber]).first.user_id
        else
            return false
        end

        entry = CsvEntry.where({user_id: user, date: values[:date]}).first_or_initialize
        entry.update_attributes({:user_id => user, :date => values[:date], :present => values[:present],
                       :missing => values[:missing], :improvment => values[:improvment]})
    end
  
  def self.save_from_csv_array(row)
    # function uses the array from a csv parser, and knows the format of the specific csv file
    
    entry = {}
    entry[:staffnumber]=row[0].to_i
    
    # entry validation (should normally done in model validation, but Date must be parsed
	date = row[1].split(".")
	
	if date.length != 3 && !(date[2].length == 2 || date[2].length == 4)
		return false
	end
	
	if date[2].length == 2
		date[2]="20" + date[2]
	end
	
	begin
		date = Date.new(date[2].to_i,date[1].to_i, date[0].to_i)  
	rescue
		return false
	end


    entry[:date]=date
    entry[:present] = (row[2] || "0").sub(',','.').to_f
    entry[:missing] = (row[3] || "0").sub(',','.').to_f
    entry[:improvment] = (row[4] || "0").sub(',','.').to_f
    
    logger.info( entry.to_s)
    self.save_with_staff_number(entry)
  
  end
end
