class SendtimereportController < ApplicationController

  def weeklyreport
    c = UserCustomField.where(:name => "Abteilung").first.try(:id)
    if !c.nil?
      users = User.joins(:custom_values).where('custom_values.custom_field_id = ? AND custom_values.value <> ""', c)
    else
      users = User.all
    end
    
    users.each do |u|
      begin
        Mailer.sendTimeReport(u, 2).deliver!
      rescue
        next
      end
    end
  end

end
