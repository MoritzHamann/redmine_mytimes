class CsvuploadController < ApplicationController
    
    #require_relative 'csvhelper'
    include CsvHelper
    
    before_filter(:only => ['index','month_times_per_cweek', 'export', 'month_times_per_day']) {|c| @date = c.valid_params_date(params)}
    before_filter :authorize_global, :except => ['export', 'month_times_per_cweek', 'month_times_per_day']
    before_filter :are_own_entries?, :only => ['export', 'month_times_per_cweek', 'month_times_per_day']

    def index
        # startseite zeigt ohne params[:date] den aktuellen monat an
        @all_entries = month_overview(@date)
    end
    
    def choose_file
    
    end
    
    def choose_radeberg_file
    
    end
    
    def month_times_per_cweek
        # this function defines a datastructure of the following kind:
        # { First_day_of_week1 => { day1 => hours_for_day1, day2 => hours_for_day2}, First_day_of_week2 =>{ ...}}

        if params[:user_id] && User.exists?(params[:user_id])
            @user = User.find(params[:user_id])
            
            # find first/last day of month, its cweek and then the first/last day of this cweek
            first_day = @date.beginning_of_month.beginning_of_week
            last_day = @date.end_of_month.end_of_week
            
            # gives us a hash of form {date => [entry1_for_date, entry2_for_date, ...]} for csv and timelog entries
            csventries = CsvEntry.by_user(@user.id).where(:date => first_day..last_day).group_by(&:date)
            timelogentries = TimeEntry.where(:user_id => @user.id, :spent_on => first_day..last_day).group_by(&:spent_on)
            
            # sum up all entries for one day
            csventries.each {|day, list_of_entries| csventries[day] = list_of_entries.sum {|e| e.hours} }
            timelogentries.each{|day, list_of_entries| timelogentries[day] = list_of_entries.sum{|e| e.hours} }
            
            
            # final data structures
            @CSV_ordered_entries = {}
            @Timelog_ordered_entries = {}
            
            first_of_week = first_day
            
            begin
            
                # create Array for that week
                @CSV_ordered_entries[first_of_week] = {}
                @Timelog_ordered_entries[first_of_week] = {}
                
                # iterate over all 7 days of the week (tree dots, to exclude last day)
                (first_of_week...(first_of_week+7)).each do |day|
                    
                    # if theres a entry in entries, use it, otherwise hours = 0
                    if csventries[day]
                        @CSV_ordered_entries[first_of_week][day] = csventries[day]
                    else
                        @CSV_ordered_entries[first_of_week][day] = 0
                    end
                    
                    # same for timelog
                    if timelogentries[day]
                        @Timelog_ordered_entries[first_of_week][day] = timelogentries[day]
                    else
                        @Timelog_ordered_entries[first_of_week][day] = 0
                    end
                end
                
                # new first_of_week ist last +7 days
                first_of_week = first_of_week + 7
                
            end until first_of_week >= last_day # if last cweek is processed
            
            # ordered_entries is now ready to render
            
        else
            flash[:error] = 'Benutzer existiert nicht'
            redirect_to :controller => 'timelog', :action => 'index'
            return
        end
    end
    
    def month_times_per_day
		if params[:user_id] && User.exists?(params[:user_id])
			@user = User.find(params[:user_id])
			@all_entries = month_detail_user(@user.id,@date)
		else
			flash[:error] = "Benutzer existiert nicht"
			redirect_to :controller => 'timelog', :action => 'index'
			return
		end
    end
    
    def export
		if params[:user_id]
			lines, filename = csv_file_detail(params[:user_id])
		else
			lines, filename = csv_file_global
		end
		
		send_data(generate_csv(lines), :type => "text/csv; header=present", :filename =>filename)
    end
    
    
    def csv_file_global
        array = []
        wanted = params[:wanted] || {}
		entries = month_overview(@date)
		array << ['Benutzer', 'Personalabteilung', 'Verbucht', 'Differenz']
		entries.each do |user, csvhours, loghours|
            if wanted[user.id.to_s] == "1"
                array << [user.to_s, csvhours.round(2).to_s, loghours.round(2).to_s, (loghours - csvhours).round(2).to_s]
            end
		end
		filename = "Uebersicht - " + @date.year.to_s + "-" + @date.month.to_s


		return array, filename
    end
    
    def csv_file_detail(user_id)
		if user = User.find_by_id(user_id)
			array = []
			
			entries = month_detail_user(user.id, @date)
			array << ['Datum','Personalabteilung','Verbucht', 'Differenz']
			
			(@date.beginning_of_month..@date.end_of_month).each do |day|
				array << [day.strftime, entries[:csv][day] || 0, entries[:timelog][day] || 0, (entries[:timelog][day] || 0.0) - (entries[:csv][day] || 0.0)]
			end
			filename = user.to_s + " - " + @date.year.to_s + "-" + @date.month.to_s
			
			return array, filename
		else
			flash[:error]="Benutzer nicht gefunden"
			redirect_to :controller => 'timelog', :action => 'index'
			return
		end
    end
    
    
    def upload
        if params[:csvfile]
            errors=[]
            i=0
            CSV.parse(params[:csvfile].read, :col_sep => ";") do |row|
				if i != 0
					if !CsvEntry.save_from_csv_array row
						errors << i
					end
				end
                i += 1
            end
            
            if errors.length > 0
                flash[:error]="Folgende Zeilen konnten nicht geschrieben werden: "+errors.join(",")
                redirect_to :action => "choose_file"
                return
            else
                flash[:notice]="Alle "+(i-1).to_s+" Zeilen wurden geschrieben"
            end
        else
            flash[:error]="Keine Datei ausgewaehlt"
            redirect_to :action => "choose_file"
            return
        end
        
        redirect_to :action => "index"
    end
    
    def upload_radeberg
      if params[:csv_file]
        errors = []
        rows = CSV.read(params[:csv_file].path, :headers => false)
      else
        flash[:error] = "Keine Datei ausgwaehlt"
        redirect_to :action => "choose_radeberg_file"
        return
      end
    end
    
    def user_mapping
		# exclude anonymous user from mapping
        @mappings = User.where("ID != ?", 2)
    end
    
    def user_mapping_detail
        if params[:user_id] && User.exists?(:id => params[:user_id])
            @user = User.find(params[:user_id])
            if !@user.Userstaffnumber
				@user.Userstaffnumber = Userstaffnumber.create(:user_id => @user.id, :staffnumber => @user.id)
			end
        else
            flash[:error]= "Benutzer nicht gefunden"
            redirect_to :action => 'user_mapping'
            return
        end
    end
    
    def edit_user_mapping
        if params[:user_id] && params[:userstaffnumber][:staffnumber] && User.exists?(params[:user_id])
            staffnumber = Userstaffnumber.where(user_id: params[:user_id]).first_or_initialize
            staffnumber.staffnumber = params[:userstaffnumber][:staffnumber]
            if staffnumber.save
				flash[:notice] = "Personalnummer aktualisiert"
				redirect_to :action => 'user_mapping'
				return
			else
				flash[:error] = "Konnte Personalnummer nicht speichern"
				redirect_to :action => 'user_mapping_detail', :user_id => params[:user_id]
				return
			end
        else
            flash[:error] = "Konnte Benutzer nicht finden"
            redirect_to :action => 'user_mapping'
            return
        end
    end
    
    def are_own_entries?
		if params[:user_id] && params[:user_id].to_i == User.current.id
			@own_entries = true
		elsif User.current.allowed_to?(:controlling, nil, :global => true)
			# if first statement is not executed, the user is not looking for its own entries
			@own_entries = false
		else
			# if user isn't looking for its own entries and is not allowed to to view csv entries, redirect to  mytimes#index
			flash[:error] = "Keine Berechtigung"
			redirect_to :controller => "timelog", :action => "index"
		end
	end
end
