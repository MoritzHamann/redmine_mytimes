module CsvHelper

  require 'csv'
  
  def valid_params_date(params)
	if params[:date]
		date = params[:date].split("-")
		date = Date.valid_date?(date[0].to_i,date[1].to_i,date[2].to_i) ? Date.new(date[0].to_i,date[1].to_i,date[2].to_i) : Date.today
	else
		date = Date.today
	end
  end
  
  def generate_csv(data)
    # generate a csv File from an array
	csv_file = CSV.generate(:col_sep => ";") do |csv|
	  data.each do |row|
		csv << row
	  end
	end
	return csv_file
  end

  def month_detail_user(user_id,date)
	# get the entries from csv and timelog for specific user grouped by date
	csv = CsvEntry.all_from_month(date).by_user(user_id).group_by(&:date)
	csv.each {|d,entries| csv[d] = entries.sum{|e| e.hours}}
	
	log = TimeEntry.where(:user_id => user_id, :tyear => date.year, :tmonth => date.month).group_by(&:spent_on)
	log.each {|d,entries| log[d] = entries.sum{|e| e.hours}}
	return {:csv => csv, :timelog => log}
  end
  
  
  def month_overview(date)
	# get all users execpt anonymous and admin
	values = User.where('id NOT IN (?)', [1,2]).map {|u| [u]}
    
	# calculate the sum of the hours from the csv entries for each user
	csv = CsvEntry.all_from_month(date).group_by(&:user)
	csv.each do |user,entries|
	  csv[user]=entries.sum {|e| e.hours}
	end
	
	# calculate the sum of the hours from the timelogs for each user
	log = TimeEntry.where(:tmonth => date.month, :tyear => date.year).group_by(&:user)
	log.each do |user, entries|
		log[user]=entries.sum {|e| e.hours}
	end
	
    values.each do |user|
        user.append(csv[user[0]] ? csv[user[0]] : 0)
        user.append(log[user[0]] ? log[user[0]] : 0)
    end

    values.sort_by{|u| u[1]}
    return values
  end

end
