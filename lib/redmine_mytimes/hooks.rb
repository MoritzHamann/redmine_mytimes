module Redmine_mytimes
  class Hooks < Redmine::Hook::ViewListener
	
    def view_layouts_base_sidebar(context={ })
      
      sidebar = ''
      controller = context[:controller]
      
      if controller && (controller.is_a?(TimelogController) || controller.is_a?(CsvuploadController))
        # only use active projects
        projects = Project.joins(:time_entries).where(:status => 1, :time_entries => {:user_id => User.current.id}).group("projects.id")
        
        sidebar << controller.send(:render_to_string,
          { :partial => "/hooks/mytimes_menu",
            :locals => { :projects => projects }})
        
        # only use active projects where user is projectleader and time is active
        projectleader = Role.where(:name => "Projektleiter").first
        if not projectleader.nil?
          projects = Project.where(:status => 1).select do |p| 
            p.users_by_role[projectleader].try(:include?, User.current) && p.module_enabled?(:time_tracking)
          end
        end
        
        sidebar << controller.send(:render_to_string,
          { :partial => "/hooks/alltimes_menu",
            :locals => { :projects => projects }})
      end
      
      return sidebar
    end

    # controller hook for timelog:create. adds variable params[:all_issues], which is needed for modified timelog:new view
    def controller_timelog_edit_before_save(context={})
      controller = context[:controller]
      if controller && controller.is_a?(TimelogController)
        project = Project.find(context[:time_entry][:project_id])
        context[:params][:all_issues] = project.nil? ? [] : project.issues.map {|i| "#{i.id} - #{i.subject}"}

        # set enddate to startdate if enddate < startdate
        enddate_id = TimeEntryCustomField.where(:name => "Enddatum").first.try(:id)
        
        if not enddate_id.nil?
          s = context[:time_entry].spent_on
          e = context[:time_entry].custom_field_value(enddate_id).try(:to_date)
          if s.is_a?(Date) and e.is_a?(Date)
            if e < s
              c = context[:time_entry].safe_attributes = {"custom_field_values" => {enddate_id.to_s => s.to_s}}
            end
          end
        end
      end
    end
  end
end
