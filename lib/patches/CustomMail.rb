# -*- coding: utf-8 -*-
module TimeReport

  def sendTimeReport(user, weeks)
    @weeks = weeks
    @logs = TimeEntry.where('user_id = ? AND spent_on >= ? AND spent_on <= ?', user.id, Date.today-weeks*7, Date.today).order('spent_on ASC')
    @csv = CsvEntry.where(:user_id => user.id).where("date >= ? AND date <= ?", Date.today-weeks*7, Date.today).order('date ASC')
    mail :to => user.mail, :subject => "Buchungsübersicht EZIS vom "+Date.today.strftime("%d.%m.%Y")
  end

end

unless Mailer.included_modules.include? TimeReport
  Mailer.send(:include, TimeReport)
end
