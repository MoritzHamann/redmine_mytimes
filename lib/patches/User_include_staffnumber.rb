require_dependency 'principal'
require_dependency 'user'

module UserPatch
    def self.included(base)
        base.class_eval do
            has_one :Userstaffnumber, :dependent => :destroy
        end
    end
end

User.send(:include, UserPatch)
