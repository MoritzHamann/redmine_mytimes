#encoding: utf-8

module TimelogPatchMultipleDays

  def self.included(base)
    base.class_eval do
      alias_method_chain :new, :multiple_days
      alias_method_chain :create, :multiple_days
    end
  end


  def new_with_multiple_days

    # get all issues for specific project. if no project is given, select first available project
    if !(@project) || @project.nil?
      if Project.allowed_to(:log_time).count < 1
        flash[:notice] = "Kein Projekt vorhanden auf dem Zeiten gebucht werden können"
      end
      @project = Project.allowed_to(:log_time).first
    end

    if @project && !@issue
      @issue = @project.issues.first
    end

    @time_entry ||= TimeEntry.new(:project => @project, :issue => @issue, :user => User.current, :spent_on => User.current.today)

    @time_entry.safe_attributes = params[:time_entry]
    if not params[:time_entry]
      department_customfield = UserCustomField.where(:name => "Abteilung").first
      department = User.current.custom_field_value(department_customfield.try(:id))
      activity_id = TimeEntryActivity.where(:name => department).first.try(:id)
      @time_entry.activity_id = activity_id
    end

    # issues for current project. Needed to provid a dropdown menu
    tracker_id = IssueCustomField.where(:name => "gesperrt").first.try(:id)
    all_issues = @project.nil? ? [] : @project.issues
    all_issues.to_a.delete_if do |issue|
      version_locked = issue.fixed_version.try(:status) == "locked"
      parents_locked = false
      if not tracker_id.nil?
        parents_locked = issue.self_and_ancestors.all.count {|parent| parent.custom_field_value(tracker_id) == "1"} > 0
      end
      version_locked || parents_locked || issue.closed?
    end
    params[:all_issues] = all_issues.map {|i| "#{i.id} - #{i.subject}"}

    # provide list of +-5 calendarweeks
    params[:calendarweeks] = (-5..5).map{|i| Date.today.beginning_of_week+i*7}
  end


  def create_with_multiple_days
    # add calendarweeks to params
    params[:calendarweeks] = (-5..5).map{|i| Date.today.beginning_of_week+i*7}

    @time_entry ||= TimeEntry.new(:project => @project, :issue => @issue, :user => User.current, :spent_on => User.current.today)
    @time_entry.safe_attributes = params[:time_entry]

    # set "Kosten/Stunde" to value of "Stundenlohn" of current user
    costs_id = TimeEntryCustomField.where(:name => "Kosten/Stunde").first.try(:id)
    salary_id = UserCustomField.where(:name => "Stundenlohn").first.try(:id)
    if costs_id and salary_id
      @time_entry.safe_attributes = {"custom_field_values" => {costs_id.to_s => User.current.custom_field_value(salary_id).to_s}}
    end

    call_hook(:controller_timelog_edit_before_save, { :params => params, :time_entry => @time_entry })

    if @time_entry.save
      respond_to do |format|
        format.html {
          flash[:notice] = l(:notice_successful_create)
          if params[:continue]
            if params[:project_id]
              options = {
                :time_entry => {:issue_id => @time_entry.issue_id, :activity_id => @time_entry.activity_id},
                :back_url => params[:back_url]
              }
              if @time_entry.issue
                redirect_to new_project_issue_time_entry_path(@time_entry.project, @time_entry.issue, options)
              else
                redirect_to new_project_time_entry_path(@time_entry.project, options)
              end
            else
              options = {
                :time_entry => {:project_id => @time_entry.project_id, :issue_id => @time_entry.issue_id, :activity_id => @time_entry.activity_id},
                :back_url => params[:back_url]
              }
              redirect_to new_time_entry_path(options)
            end
          else
            redirect_back_or_default project_time_entries_path(@time_entry.project)
          end
        }
        format.api  { render :action => 'show', :status => :created, :location => time_entry_url(@time_entry) }
      end
    else
      respond_to do |format|
        format.html { render :action => 'new' }
        format.api  { render_validation_errors(@time_entry) }
      end
    end
  end

  def valid_date(date_string)
    # uses date string with following format:
    #     "2012-10-25"
    # and returns Date object of nil depending if format is valid

    if date_string.blank?
      return nil
    end

    if date_string && date_string.split("-").first.length != 4
      return nil
    end

    begin
      return date = Date.strptime(date_string, "%Y-%m-%d")
    rescue
      return nil
    end
  end
end


module TimeEntryPresenceIssue
	# ensures that issue_id is given and not nil
	def self.included(base)
		base.class_eval do
			validates_presence_of :issue_id
		end
	end
end

#module TimelogPatchTimeScope

    #def self.included(base)
        #base.class_eval do
            #alias_method_chain :time_entry_scope, :user_filter
        #end
    #end

    #def time_entry_scope_with_user_filter(options={})
        ## insert custom field for enddate in query for table
        #id = TimeEntryCustomField.where(:name => "Enddatum").first.try(:id)
        #columns = @query.column_names || @query.default_columns_names
        #index = columns.index(:spent_on) || columns.length-1
        #if id
          #columns.insert(index+1, ("cf_"+id.to_s).to_sym)
          #@query.column_names = columns
        #end

        #scope = TimeEntry.visible.where(@query.statement)
        #if @issue
            #scope = scope.on_issue(@issue)
        #elsif @project
            #include_subprojects = true
            #scope = scope.includes(:project).where(@project.project_condition(include_subprojects))
            ##scope = scope.on_project(@project, Setting.display_subprojects_issues?)
        #end
        #scope = scope.where(:user_id => User.current.id) unless (User.current.allowed_to?(:edit_time_entries, @project) ||
                                                                 #User.current.allowed_to?(:controlling, nil, :global => true))
        #scope
    #end
#end

module TimeEntryQueryIncludeEnddate
  def self.included(base)
    base.class_eval do
      alias_method_chain :results_scope, :enddate
    end
  end

  def results_scope_with_enddate(options = {})
    id = TimeEntryCustomField.where(:name => "Enddatum").first.try(:id)
    columns = self.column_names || self.default_columns_names
    index = columns.index(:spent_on) || columns.length-1
    if id
      columns.insert(index+1, ("cf_"+id.to_s).to_sym)
      self.column_names = columns
    end

    scope = nil

    if User.current.allowed_to?(:controlling, nil, :global => true)
      scope = TimeEntry.joins(:project).where(self.statement)
    else
      scope = results_scope_without_enddate(options)
    end

    scope
  end
end

module TimeEntryCustomValidation

  def self.included(base)
    base.class_eval do
      alias_method_chain :validate_time_entry, :custom_validations
    end
  end

  def validate_time_entry_with_custom_validations
    errors.add :hours, :invalid if hours && (hours < 0 || hours >= 1000)
    errors.add :project_id, :invalid if project.nil?
    errors.add :issue_id, :invalid if (issue_id && !issue) || (issue && project!=issue.project)
    # errors[:base] << "Aufabe ist bereits abgeschlossen" if issue.closed?
    errors[:base] << "Meilenstein für diese Aufgabe ist gesperrt" if issue.fixed_version.try(:status) == "locked"
    errors[:base] << "Konnte nicht gespeichert werden, da Aufgabe kein Aufwand hat" if issue.estimated_hours == nil

    tracker_id = IssueCustomField.where(:name => "gesperrt").first.try(:id)
    if not tracker_id.nil?
			if issue.custom_field_value(tracker_id) == "1"
				errors[:base] << "Aufgabe ist gesperrt"
			end
		end
  end
end


unless TimelogController.included_modules.include? TimelogPatchMultipleDays
	TimelogController.send(:include, TimelogPatchMultipleDays)
	#TimelogController.send(:include, TimelogPatchTimeScope)
end

unless TimeEntryQuery.included_modules.include? TimeEntryQueryIncludeEnddate
  TimeEntryQuery.send(:include, TimeEntryQueryIncludeEnddate)
end

unless TimeEntry.included_modules.include? TimeEntryPresenceIssue
	TimeEntry.send(:include, TimeEntryPresenceIssue)
end

unless TimeEntry.included_modules.include? TimeEntryCustomValidation
  TimeEntry.send(:include, TimeEntryCustomValidation)
end
