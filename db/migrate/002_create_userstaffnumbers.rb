class CreateUserstaffnumbers < ActiveRecord::Migration
  def change
    create_table :userstaffnumbers do |t|
      t.integer :user_id
      t.integer :staffnumber
    end
  end
end
