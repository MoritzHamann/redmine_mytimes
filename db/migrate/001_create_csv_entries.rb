class CreateCsvEntries < ActiveRecord::Migration
  def change
    create_table :csv_entries do |t|
      t.integer :user_id
      t.date  :date
      t.float :present
      t.float :missing
      t.float :improvment
    end
  end
end
