Redmine::Plugin.register :redmine_mytimes do
  name 'Extended Time Tracking'
  author 'Moritz Hamann'
  description 'This plugin adds a page for personal time records, as well as a new tab as overview for all time tracks'
  version '0.0.3'
  
  require_dependency 'redmine_mytimes/hooks'
  
  if ActiveRecord::Base.connection.table_exists? 'custom_fields'
    if UserCustomField.where(:name => "Abteilung").empty?
      UserCustomField.create({:type => "UserCustomField", :name => "Abteilung", :field_format => "list",
          :possible_values => ["Mikroelektronik", "Konstruktion", "Elektronik Design", "Leistungselektronik"],
          :is_filter => true, :editable => true, :visible => true})
    end

    if TimeEntryCustomField.where(:name => "Enddatum").empty?
      TimeEntryCustomField.create({:name => "Enddatum", :field_format => 'date', :is_required => false, :is_filter => true,
                                    :searchable => true, :editable => true, :visible => true})
    end

    if TimeEntryCustomField.where(:name => "Kosten/Stunde").empty?
      TimeEntryCustomField.create({:name => "Kosten/Stunde", :field_format => "float", :is_required => false, :is_filter => false,
                                   :searchable => false, :editable => false, :visible => false, :default_value => 70})
    end
    
    if IssueCustomField.where(:name => "gesperrt").empty?
			t = Tracker.where(:name => "Aufgabe").first
			if not t.nil?
				c = IssueCustomField.create({:name => 'gesperrt', :field_format => "bool", :is_required => false, :is_filter => true,
										      					 :searchable => true, :editable => "0", :default_value => false, :is_for_all => true})
				c.trackers.push(t)
				c.save
			end
		end
  end

  require 'dispatcher' unless Rails::VERSION::MAJOR >= 3
 
	if Rails::VERSION::MAJOR >= 3
		ActionDispatch::Callbacks.to_prepare do
			# use require_dependency if you plan to utilize development mode
			require 'patches/User_include_staffnumber'
			require 'patches/timelog_new_create_scope'
                        require 'patches/CustomMail'
		end
	else
		Dispatcher.to_prepare BW_AssetHelpers::PLUGIN_NAME do
			# use require_dependency if you plan to utilize development mode
			require 'patches/User_include_staffnumber'
			require 'patches/timelog_new_create_scope'
                        require 'patches/CustomMail'
		end
	end
  
  
  project_module :time_tracking do
    permission :controlling, :csvupload => [:index, :upload, :export, :choose_file, :user_mapping, :user_mapping_detail, :edit_user_mapping]
  end
  
  menu :project_menu, :mytimes, { :controller => 'timelog', :action => 'index' }, :caption => 'Gebuchte Zeiten', 
       :last => true, :param => :project_id, :if => Proc.new {User.current.logged? && User.current.allowed_to?(:view_time_entries, nil, :global=>true)}
  menu :account_menu, :mytimes, { :controller => 'timelog', :action => 'index' }, 
       :caption => 'Meine Zeiten', :after => :my_account, 
       :if => Proc.new {User.current.logged? && User.current.allowed_to?(:view_time_entries, nil, :global=>true)}
end


